#@extend

Simply put, this can be used to set a core set of styles; if we add additional styles that  
require minor changes, we can extend them without creating any additional bloat.

###Example:

Let's hive off the common styles into a new style and add two new styles that will deal with  
the difference by extending the **common** .couch style:

```sass
.couch {
  padding: 2em;
  height: 246rem;
  width: 528rem;

}

.couch-leather {
  @extend .couch;
  background: saddlebrown;
}

.couch-fabric {
  @extend .couch;
  background: linen;
}
```

If we compile the preceding code, we should get the following:

```sass
.couch, .couch-leather, .couch-fabric {
  padding: 2em;
  height: 37in;
  width: 88in;
  z-index: 40;
  }

.couch-leather {
  background: saddlebrown;
}

.couch-fabric {
  background: linen;
}
```

###Note:
In some respects, it is better to use @extend to create new styles, rather than a mixin. Using  
a mixin will work perfectly well, but it requires care and attention to ensure that we're only  
using those attributes that we need, and not any that will lead to unnecessary bloat in our code.
module.exports = function (grunt) {

	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		express: {
			all: {
				options: {
					bases: ['C:\Users\jm5256\Source\Repos\sass'], // /Users/Ivan/Dropbox/Development/Repos/SASS
					port: 8080,
					hostname: "0.0.0.0",
					livereload: true
				}
			}
		},
		watch: {
			all: {
				files: '**/*.html',
				options: {
					livereload: true
				}
			},
			sass: {
				files: ['sass/**/*.{scss,sass}', 'sass/_partials/**/*.{scss,sass}'],
				tasks: ['sass:dist']
			},
			livereload: {
				files: ['*.html', '*.php', 'js/**/*.{js,json}', 'css/*.css', 'img/**/*.{png,jpg,jpeg,gif,webp,svg}'],
				options: {
					livereload: true
				}
			}
		},
		sass: {
			dist: {
				options: {
					sourcemap: 'auto',
					style: 'expanded', //use 'compressed' for production use.
					lineNumbers: true
				},
				files: [{
					expand: true, // Recursive
					cwd: "sass", // The startup directory
					src: ["**/*.scss"], // Source files
					dest: "css", // Destination
					ext: ".css" // File extension
				}]
			}
		},
		open: {
			all: {
				path: 'http://localhost:8080/index.html'
			}
		}
	});	
	grunt.registerTask('init', ['express', 'open', 'sass:dist','watch']);
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
};
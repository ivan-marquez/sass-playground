#@import

###Example

```sass
@import "_partials/variables";
@import "_partials/typography";
@import "_partials/elements";
@import "_vendor/grid";
@import "_vendor/mixins";
```

It's worth noting that we can use other formats too, such as importing multiple   
files into one statement, as shown in the following code:

```sass
@import "_partials/variables",
        "_partials/typography",
        "_vendor/grid";
```

We can also import multiple files in one go, but separated on a per line basis:

```sass
@import "_partials/variables",
        "_partials/typography",
        "_vendor/grid";
```        
        
Either method will work. It's a matter of personal preference as to which method you use.
  
The trick is to ensure that we choose the right mixin library to use (to avoid bloating)  
or extract the mixins we need and discard the rest of the library.

#Importing Partials

```sass
@import "_partials/_colors.scss";
```

The underscore in the filename means that is a **partial**. It's easiest to think of this as a code exerpt  
that we will put in our main style sheet. The key point though is that there`s no need to compile it in  
a standalone file in its own right.
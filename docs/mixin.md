#@mixin

Mixins do as the name suggests. They allow you to "mix-in" blocks of code into  
our existing projects, which reduces the amount of code we need to write because  
the work has already been done.

```sass
@mixin large-text {
  font: {
    family: Arial;
    size: 20px;
    weight: bold;
  }
  color: #ff0000;
}
```

The great thing about mixins is that we can easily reference them within other rules in our code.

```sass
.page-title {
  @include large-text;
  padding: 4px;
  margin-top: 10px;
}
```

#Passing arguments to mixins

In place of standard mixins with fixed values, we can use parametric mixins, where we pass it to different  
values in each instance of calling the mixin.

```sass
@mixin image-template($height, $width) {
  border: {
    border-radius: 4px;
    height: $height;
    width: $width;
  }
}
.imgMedium { 
  @include image-template(5rem, 7rem); 
  }
```

